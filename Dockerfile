FROM openjdk:17-slim as jar-builder
RUN apt-get update && apt-get install -y \
    curl \
    unzip \
    zip \
    ca-certificates \
    gnupg \
    default-jdk

RUN curl -s https://get.sdkman.io | bash

COPY . /app
RUN chmod 775 /app/gradlew
RUN cd /app && \
    ./gradlew build -x test

WORKDIR /app
ENTRYPOINT ["./gradlew", "test"]

FROM openjdk:17-slim as app

COPY --from=jar-builder /app/build/libs/PartySwipe-0.0.1-SNAPSHOT.jar /app/app.jar

EXPOSE 8000
COPY entrypoint.sh /
RUN chmod 775 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]