import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import tech.party.swipe.controller.CategoryController
import tech.party.swipe.dto.CategoryDto
import tech.party.swipe.security.JWTToken
import tech.party.swipe.service.CategoryService
import tech.party.swipe.service.UserService

@ExtendWith(MockitoExtension::class)
class CategoryControllerTest {
    @Mock
    lateinit var userService: UserService

    @Mock
    lateinit var categoryService: CategoryService

    @InjectMocks
    lateinit var categoryController: CategoryController

    private lateinit var mockMvc: MockMvc

    @BeforeEach
    fun setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(categoryController).build()
    }

    @Test
    fun getAllCategories() {
        `when`(categoryService.getAllCategories()).thenReturn(
            listOf(
                CategoryDto(1, "kitties", listOf()),
                CategoryDto(2, "korean street food", listOf()),
                CategoryDto(3, "french films", listOf()),
            ),
        )

        mockMvc.perform(get("/categories"))
            .andExpect(status().isOk())
    }

    @Test
    fun getUserCategories() {
        // TODO
        `when`(userService.getUserCategories()).thenReturn(
            listOf(
                CategoryDto(1, "kitties", listOf()),
                CategoryDto(2, "korean street food", listOf()),
            ),
        )

        mockMvc.perform(get("/user-categories").principal(getAuthentication()))
            .andExpect(status().isOk)
            .andExpect(content().string("[\"kitties\",\"korean street food\"]"))
    }

    @Test
    fun addUserCategoryNotFoundTest() {
        // TODO
//        `when`(userService.addUserCategory("capybara")).thenReturn(
//            ResponseEntity.notFound().build()
//        )

        mockMvc.perform(post("/user-categories/{category}", "capybara").principal(getAuthentication()))
            .andExpect(status().isNotFound)
    }

    @Test
    fun addUserCategoryOkTest() {
        // TODO
//        `when`(userService.addUserCategory("kitties")).thenReturn(
//            ResponseEntity.ok().build()
//        )

        mockMvc.perform(post("/user-categories/{category}", "kitties").principal(getAuthentication()))
            .andExpect(status().isOk)
    }

    @Test
    fun deleteUserCategoryOkTest() {
        // TODO
//        `when`(userService.deleteUserCategory("kitties")).thenReturn(
//            ResponseEntity.ok().build()
//        )

        mockMvc.perform(delete("/user-categories/{category}", "kitties").principal(getAuthentication()))
            .andExpect(status().isOk)
    }

    @Test
    fun deleteUserCategoryNotFoundTest() {
        // TODO
//        `when`(userService.deleteUserCategory("capybara")).thenReturn(
//            ResponseEntity.notFound().build()
//        )

        mockMvc.perform(delete("/user-categories/{category}", "capybara").principal(getAuthentication()))
            .andExpect(status().isNotFound)
    }

    private fun getAuthentication(): UsernamePasswordAuthenticationToken {
        val authorities = mutableListOf(SimpleGrantedAuthority("ROLE_USER"))
        val jwtMock = createMockJwt()
        val jwtToken = JWTToken(jwtMock, "mockedFirebaseUserId")

        val authentication = UsernamePasswordAuthenticationToken("mockedUser", "mockedPassword", authorities)
        authentication.details = jwtToken

        return authentication
    }

    private fun createMockJwt(): Jwt {
        return mock(Jwt::class.java)
    }
}
