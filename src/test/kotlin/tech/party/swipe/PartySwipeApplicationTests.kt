package tech.party.swipe

import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.*
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureEmbeddedDatabase(refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class PartySwipeApplicationTests
