import jakarta.persistence.EntityManagerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration

@TestConfiguration
class TestConfiguration {
    @Autowired
    private lateinit var entityManagerFactory: EntityManagerFactory
}
