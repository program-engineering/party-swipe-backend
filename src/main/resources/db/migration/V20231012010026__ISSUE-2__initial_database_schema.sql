CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       email VARCHAR(255) NOT NULL UNIQUE,
                       date_of_birth DATE NOT NULL
);

CREATE TABLE category (
                            id SERIAL PRIMARY KEY,
                            name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE user_category (
                                 user_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
                                 category_id INTEGER REFERENCES category(id) ON DELETE CASCADE,
                                 PRIMARY KEY (user_id, category_id)
);

CREATE TABLE event (
                        id SERIAL PRIMARY KEY,
                        location TEXT NOT NULL,
                        date TIMESTAMPTZ  NOT NULL DEFAULT now(),
                        title TEXT NOT NULL,
                        description TEXT NOT NULL,
                        picture TEXT,
                        name TEXT NOT NULL
);

CREATE TABLE match (
                         id SERIAL PRIMARY KEY,
                         user_id1 INTEGER NOT NULL REFERENCES users(id),
                         user_id2 INTEGER NOT NULL REFERENCES users(id),
                         event_id INTEGER NOT NULL REFERENCES event(id),
                         UNIQUE(user_id1, user_id2, event_id)
);
