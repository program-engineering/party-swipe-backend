alter table "event" drop column if exists "title";

CREATE TABLE event_category (
     event_id INTEGER REFERENCES event(id) ON DELETE CASCADE,
     category_id INTEGER REFERENCES category(id) ON DELETE CASCADE,
     PRIMARY KEY (event_id, category_id)
);