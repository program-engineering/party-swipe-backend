insert into "users" (id, "email", date_of_birth, "name", firebase_id, bio, image_url) values
    (1, 'kigorleo@gmail.com', '2002-11-12', 'KawaiiLapen', 'utRVaryD1phhGCnTtHtDG5VT2OJ3', 'just lapen',
    'https://storage.yandexcloud.net/party-swipe-bucket/avatars/anime-guy.webp'),
    (2, 'andreiyaremenko9@gmail.com', '2011-11-11', 'Andrew Yaremenko', 'FDmniT1KjbhDC8H17N4XG4QTyK73', 'love await',
    'https://storage.yandexcloud.net/party-swipe-bucket/avatars/anime-girl.webp');

insert into user_category (user_id, category_id) values
    (1, 12),
    (1, 1),
    (1, 4),
    (2, 1),
    (2, 4),
    (2, 8);

insert into "event" (id, "location", "date", "description", "picture", "name", "owner_id") values
    (1, '14 line of Vasilievskiy island, Saint-Petersburg, Russia', '2023-12-26', 'where they will know even more, then the developer',
     'https://storage.yandexcloud.net/party-swipe-bucket/events/party-event.webp', 'PE exam by lapen', 1),
    (2, '14 line of Vasilievskiy island, Saint-Petersburg, Russia', '2023-12-26', 'where they will know even more, then the developer',
     'https://storage.yandexcloud.net/party-swipe-bucket/events/chess-event.webp', 'PE exam by await', 2),
    (3, '14 line of Vasilievskiy island, Saint-Petersburg, Russia', '2023-12-26', 'Relax, have fun and watch one piece!',
         'https://storage.yandexcloud.net/party-swipe-bucket/events/dating-anime.webp', 'Anime party', 2),
    (4, '14 line of Vasilievskiy island, Saint-Petersburg, Russia', '2022-12-26', 'Best bavarian beer just from Germany!',
         'https://storage.yandexcloud.net/party-swipe-bucket/avatars/beer-lovers.webp', 'Bavarian beer party', 2),
    (5, '14 line of Vasilievskiy island, Saint-Petersburg, Russia', '2023-12-26', 'Best bavarian beer just from Germany!',
         'https://storage.yandexcloud.net/party-swipe-bucket/avatars/beer-lovers.webp', 'Bavarian beer party', 2);

insert into event_category (event_id, category_id) values
    (1, 1),
    (1, 4),
    (1, 5),
    (2, 1),
    (2, 4),
    (2, 5),
    (3, 12),
    (3, 9),
    (3, 10),
    (4, 10),
    (4, 8),
    (4, 7),
    (5, 10),
    (5, 8),
    (5, 7);