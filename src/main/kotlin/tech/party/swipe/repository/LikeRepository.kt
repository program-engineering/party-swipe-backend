package tech.party.swipe.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import tech.party.swipe.entity.Like

@Repository
interface LikeRepository : CrudRepository<Like, Long> {
    fun findAllByEventId(id: Long): List<Like>
    fun findAllByUserId(id: Long): List<Like>
}
