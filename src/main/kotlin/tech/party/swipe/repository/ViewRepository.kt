package tech.party.swipe.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import tech.party.swipe.entity.View

@Repository
interface ViewsRepository : CrudRepository<View, Long> {
    fun findAllByEventId(id: Long): List<View>
    fun findAllByUserId(id: Long): List<View>
}
