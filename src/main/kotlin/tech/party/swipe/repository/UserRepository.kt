package tech.party.swipe.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import tech.party.swipe.entity.User
import java.util.*

@Repository
interface UserRepository : CrudRepository<User, Long> {
    fun findByFirebaseId(firebaseId: String): User?
}
