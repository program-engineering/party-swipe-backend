package tech.party.swipe.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import tech.party.swipe.entity.Event

@Repository
interface EventRepository : CrudRepository<Event, Long> {
    fun getById(id: Long): Event
    fun getAllByOwnerId(id: Long): List<Event>

    fun deleteEventsById(id: Long): Event?
}
