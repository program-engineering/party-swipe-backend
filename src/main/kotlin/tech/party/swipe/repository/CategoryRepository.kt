package tech.party.swipe.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import tech.party.swipe.entity.Category

@Repository
interface CategoryRepository : CrudRepository<Category, Long> {
    fun getCategoriesByName(name: String): Category?
}
