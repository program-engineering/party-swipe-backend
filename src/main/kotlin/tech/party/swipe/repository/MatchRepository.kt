package tech.party.swipe.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import tech.party.swipe.entity.Match
import tech.party.swipe.entity.User

@Repository
interface MatchRepository : CrudRepository<Match, Long> {
    fun findAllByUser1(user: User): List<Match>
    fun findAllByUser2(user: User): List<Match>
}
