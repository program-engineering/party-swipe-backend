package tech.party.swipe.entity

import jakarta.persistence.*
import tech.party.swipe.dto.MatchDto

@Entity
@Table(name = "match")
class Match(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @ManyToOne
    @JoinColumn(name = "user_id1")
    val user1: User,

    @ManyToOne
    @JoinColumn(name = "user_id2")
    val user2: User,

    @ManyToOne
    @JoinColumn(name = "event_id")
    val event: Event,
) {
    fun toDto(): MatchDto = MatchDto(
        id = id,
        user1 = user1.toDto(),
        user2 = user2.toDto(),
        event = event.toDto(),
    )
}
