package tech.party.swipe.entity

import jakarta.persistence.*
import tech.party.swipe.dto.ViewDto

@Entity
@Table(name = "views")
class View(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @ManyToOne
    @JoinColumn(name = "event_id")
    val event: Event,

    @ManyToOne
    @JoinColumn(name = "user_id")
    val user: User,
) {
    fun toDto(): ViewDto {
        return ViewDto(
            id = id,
            event = event.toDto(),
            user = user.toDto(),
        )
    }
}
