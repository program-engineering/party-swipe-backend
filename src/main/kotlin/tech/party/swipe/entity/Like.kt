package tech.party.swipe.entity

import jakarta.persistence.*
import tech.party.swipe.dto.LikeDto

@Entity
@Table(name = "likes")
class Like(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    val event: Event,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    val user: User,
) {
    fun toDto(): LikeDto {
        return LikeDto(
            id = id,
            event = event.toDto(),
            user = user.toDto(),
        )
    }
}
