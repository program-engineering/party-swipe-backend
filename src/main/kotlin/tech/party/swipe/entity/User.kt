package tech.party.swipe.entity

import jakarta.persistence.*
import tech.party.swipe.dto.UserDto
import java.time.LocalDate
import java.util.*

@Entity
@Table(name = "users")
class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(name = "email", unique = true, nullable = false)
    val email: String,

    @Column(name = "date_of_birth", nullable = false)
    val dateOfBirth: LocalDate,

    @Column(name = "firebase_id", nullable = false)
    val firebaseId: String,

    @Column(name = "name", nullable = false)
    val name: String,

    @ManyToMany
    @JoinTable(
        name = "user_category",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "category_id")],
    )
    val categories: List<Category> = listOf(),

    @Column(name = "image_url", nullable = true)
    val imageUrl: String? = null,

    @Column(name = "bio", nullable = true)
    val bio: String = "С пивом к счастью!",
) {
    fun toDto(): UserDto {
        return UserDto(
            id = id,
            email = email,
            dateOfBirth = dateOfBirth,
            firebaseId = firebaseId,
            name = name,
            categories = categories.map { it.toDto() }.toMutableSet(),
            imageUrl = imageUrl,
            bio = bio,
        )
    }
}
