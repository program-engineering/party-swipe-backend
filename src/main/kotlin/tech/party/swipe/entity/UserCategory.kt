package tech.party.swipe.entity

import jakarta.persistence.*

@Entity
@Table(name = "user_category")
data class UserCategory(
    @Id
    @ManyToOne
    @JoinColumn(name = "user_id")
    val user: User,

    @Id
    @ManyToOne
    @JoinColumn(name = "category_id")
    val category: Category,
)
