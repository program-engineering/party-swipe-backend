package tech.party.swipe.entity

import jakarta.persistence.*
import tech.party.swipe.dto.CategoryDto

@Entity
@Table(name = "category")
class Category(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(name = "name", unique = true, nullable = false)
    val name: String,
) {
    fun toDto(): CategoryDto {
        return CategoryDto(
            id = id,
            name = name,
        )
    }
}
