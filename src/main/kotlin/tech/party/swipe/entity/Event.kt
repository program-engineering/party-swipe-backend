package tech.party.swipe.entity

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import lombok.Getter
import tech.party.swipe.dto.EventDto
import java.time.LocalDateTime

@Entity
@Table(name = "event")
@Getter
@EqualsAndHashCode
class Event(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(name = "location", nullable = false)
    val location: String,

    @Column(name = "date", nullable = false)
    val date: LocalDateTime,

    @Column(name = "description", nullable = false)
    val description: String,

    @Column(name = "picture")
    val picture: String,

    @Column(name = "name", nullable = false)
    val name: String,

    @ManyToOne
    @JoinColumn(name = "owner_id")
    val owner: User,

    @ManyToMany
    @JoinTable(
        name = "event_category",
        joinColumns = [JoinColumn(name = "event_id")],
        inverseJoinColumns = [JoinColumn(name = "category_id")],
    )
    val categories: List<Category>,
) {
    fun toDto(): EventDto = EventDto(
        id = id,
        name = name,
        image_id = picture,
        date = date,
        locate = location,
        description = description,
        categories = categories.map { it.toDto() },
        owner = owner.toDto(),
    )
}
