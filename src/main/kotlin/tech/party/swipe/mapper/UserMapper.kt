package tech.party.swipe.mapper

import org.mapstruct.Mapper
import tech.party.swipe.dto.UserDto
import tech.party.swipe.entity.User

@Mapper
interface UserMapper {
    fun toDto(user: User): UserDto

    fun toBean(userDTO: UserDto): User
}
