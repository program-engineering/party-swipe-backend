package tech.party.swipe.mapper

import tech.party.swipe.entity.Event
import tech.party.swipe.restdata.EventReturnResponse

object EventMapper {
    fun entityToEventDto(event: Event): tech.party.swipe.dto.EventDto {
        return tech.party.swipe.dto.EventDto(
            id = event.id,
            name = event.name,
            image_id = event.picture,
            date = event.date,
            locate = event.location,
            description = event.description,
            categories = event.categories.map { it -> it.toDto() },
            owner = event.owner.toDto(),
        )
    }

    @Deprecated("now with views")
    fun entityToEventReturnResponse(event: Event): EventReturnResponse {
        return EventReturnResponse(
            id = event.id!!,
            name = event.name,
            image_id = event.picture,
            date = event.date,
            locate = event.location,
            discript = event.description,
            tags = emptyList(),
            ownerId = event.owner.id!!,
            countLikes = 0,
        )
    }

    fun entityToEventReturnResponse(event: Event,views: Int) : EventReturnResponse{
        val response = entityToEventReturnResponse(event)
        response.addViews(views)
        return response
    }
}
