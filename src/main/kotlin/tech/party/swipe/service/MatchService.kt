package tech.party.swipe.service

import org.springframework.stereotype.Service
import tech.party.swipe.dto.EventDto
import tech.party.swipe.dto.MatchDto
import tech.party.swipe.dto.UserDto
import tech.party.swipe.entity.Match
import tech.party.swipe.repository.*
import tech.party.swipe.restdata.MatchResponse
import tech.party.swipe.utils.MatchConstants.matchNum

@Service
class MatchService(
    private val matchRepository: MatchRepository,
    private val eventRepository: EventRepository,
    private val viewRepository: ViewsRepository,
    private val likeRepository: LikeRepository,
    private val userService: UserService,
) {
    fun getUserMatches(): List<MatchResponse> {
        val userDto = userService.findUserByFirebase()

        val matches = matchRepository.findAllByUser1(userDto.toEntity()).map { it.toDto() }

        if (matches.size < matchNum) {
            val events = findUnseenEvents(userDto)
            if (events.size >= matchNum) {
                return findUserMatches(userDto, events).map { it.toResponse(it.event.id?.let{viewRepository.findAllByEventId(it).size}?:0) }
            }
        }

        return matches.map { it.toResponse(it.event.id?.let{viewRepository.findAllByEventId(it).size}?:0) }
    }

    fun getAllUserMatches() {
        val users = userService.findAllUsers()

        users.forEach { user ->
            findUserMatches(user = user)
        }
    }

    private fun findUserMatches(
        user: UserDto,
        unseenEvents: List<EventDto>? = null,
        userMatches: List<MatchDto>? = null,
    ): List<MatchDto> {
//        val userMatchedEvents = userMatches ?: matchRepository.findAllByUser1(user =
//        user.toEntity()).map { it.event.toDto().id }
        val allEvents = unseenEvents ?: findUnseenEvents(user)
//        val unmatchedEvents = allEvents.sortedBy { event ->
//            event.categories.sumOf { user.categories.contains(it).toString().toInt() }
//        }

        val matches = allEvents.take(matchNum).map {
            matchRepository.save(
                Match(
                    user1 = user.toEntity(),
                    user2 = it.owner.toEntity(),
                    event = it.toEntity(),
                ),
            ).toDto()
        }

        return matches
//        val userMatchedEvents = userMatches ?: matchRepository.findAllByUser1(user =
//        user.toEntity()).map { it.event.toDto().id }
//        val allEvents = unseenEvents ?: findUnseenEvents(user)
//        val unmatchedEvents = if (userMatchedEvents.isNotEmpty()) {
//            allEvents.filter { event -> !userMatchedEvents.contains(event.id) }
//        } else {
//            allEvents
//        }.sortedBy { event ->
//            event.categories.sumOf { category -> user.categories.contains(category).toString().toInt() }
//        }
//
//        val matches = unmatchedEvents.take(matchNum).map {
//            matchRepository.save(
//                Match(
//                    user1 = user.toEntity(),
//                    user2 = it.owner.toEntity(),
//                    event = it.toEntity(),
//                ),
//            ).toDto()
//        }
//
//        return matches
    }

    private fun findUnseenEvents(user: UserDto): List<EventDto> {
        val viewedEvents = viewRepository.findAllByUserId(user.id!!).map { it.event.toDto() }
        val likedEvents = likeRepository.findAllByUserId(user.id).map { it.event.toDto() }
        val allEvents = eventRepository.findAll().map { it.toDto() }.filter {
            it.owner.id != user.id &&
                !viewedEvents.contains(it) && !likedEvents.contains(it)
        }

        return allEvents
    }
}
