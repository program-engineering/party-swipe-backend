package tech.party.swipe.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import tech.party.swipe.dto.LikeDto
import tech.party.swipe.dto.ViewDto
import tech.party.swipe.exception.EventNotFoundException
import tech.party.swipe.mapper.EventMapper.entityToEventReturnResponse
import tech.party.swipe.repository.EventRepository
import tech.party.swipe.repository.LikeRepository
import tech.party.swipe.repository.ViewsRepository
import tech.party.swipe.restdata.EventReturnResponse

@Service
class EventReactionService(
    private val likeRepository: LikeRepository,
    private val viewsRepository: ViewsRepository,
    private val eventRepository: EventRepository,
    private val userService: UserService,
) {
    fun likeEvent(eventId: Long) {
        val userDto = userService.findUserByFirebase()
        val eventDto = eventRepository.findByIdOrNull(eventId)?.toDto() ?: throw EventNotFoundException()

        likeRepository.save(
            LikeDto(
                event = eventDto,
                user = userDto,
            ).toEntity(),
        )
    }

    fun viewEvent(eventId: Long) {
        val userDto = userService.findUserByFirebase()
        val eventDto = eventRepository.findByIdOrNull(eventId)?.toDto() ?: throw EventNotFoundException()

        viewsRepository.save(
            ViewDto(
                event = eventDto,
                user = userDto,
            ).toEntity(),
        )
    }

    fun getLikedEvents(): List<EventReturnResponse> {
        val userDto = userService.findUserByFirebase()
        val likes = likeRepository.findAllByUserId(userDto.id!!)

        return likes.map { entityToEventReturnResponse(it.event,it.event.id?.let { viewsRepository.findAllByEventId(it).size }?:0) }
    }
}
