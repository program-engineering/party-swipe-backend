package tech.party.swipe.service

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import tech.party.swipe.dto.CategoryDto
import tech.party.swipe.dto.UserDto
import tech.party.swipe.dto.UserRegisterDto
import tech.party.swipe.entity.User
import tech.party.swipe.exception.CategoryNotFoundException
import tech.party.swipe.exception.ImageUrlNotFoundException
import tech.party.swipe.exception.UserNotFoundException
import tech.party.swipe.repository.CategoryRepository
import tech.party.swipe.repository.UserRepository
import tech.party.swipe.restdata.SwitchCategoryResponse
import tech.party.swipe.restdata.UserFullResponse
import tech.party.swipe.restdata.UserResponseData
import tech.party.swipe.security.JWTToken
import tech.party.swipe.utils.UserUtils.extractFirebaseIdFromSecurityContext

@Service
class UserService(
    private val userRepository: UserRepository,
    private val categoryRepository: CategoryRepository,
) {

    fun addUser(request: UserRegisterDto): UserResponseData =
        userRepository.save(
            User(
                email = extractUserEmail(),
                dateOfBirth = request.dateOfBirth,
                firebaseId = extractFirebaseIdFromSecurityContext(),
                name = request.name,
                categories = listOf(),
            ),
        ).toDto().toResponse()

    fun findUserByFirebase(): UserDto {
        val firebaseId = extractFirebaseIdFromSecurityContext()
        return userRepository.findByFirebaseId(firebaseId)?.toDto() ?: throw UserNotFoundException()
    }

    fun extractUserEmail(): String {
        return (SecurityContextHolder.getContext().authentication as JWTToken).tokenAttributes["email"].toString()
    }

    @Transactional
    fun addUserCategory(categoryName: String): String {
        val userDTO = findUserByFirebase()
        val category = categoryRepository.getCategoriesByName(categoryName) ?: throw CategoryNotFoundException()

        userDTO.categories.add(category.toDto())
        userRepository.save(userDTO.toEntity())

        return categoryName
    }

    @Transactional
    fun deleteUserCategory(categoryName: String): String {
        val userDTO = findUserByFirebase()

        if (!userDTO.categories.map { category -> category.name }.contains(categoryName)) {
            throw CategoryNotFoundException()
        }

        userDTO.categories.removeIf { category -> category.name == categoryName }
        userRepository.save(userDTO.toEntity())

        return categoryName
    }

    fun findAllUsers(): List<UserDto> = userRepository.findAll().map { it.toDto() }

    fun getUserCategories(): List<CategoryDto> {
        return findUserByFirebase().categories
            .map { category ->
                CategoryDto(
                    id = category.id,
                    name = category.name,
                )
            }
    }

    @Transactional
    fun addUserImageUrl(imageUrl: String): String {
        val userDto = findUserByFirebase()

        userDto.imageUrl = imageUrl
        userRepository.save(userDto.toEntity())

        return imageUrl
    }

    @Transactional
    fun deleteUserImageUrl(): String? {
        val userDto = findUserByFirebase()
        val deletedUrl = userDto.imageUrl ?: throw ImageUrlNotFoundException()

        userDto.imageUrl = null
        userRepository.save(userDto.toEntity())

        return deletedUrl
    }

    @Transactional
    fun updateUserImageUrl(imageUrl: String): String {
        val userDto = findUserByFirebase()

        userDto.imageUrl = imageUrl
        userRepository.save(userDto.toEntity())

        return imageUrl
    }

    fun getUserImageUrl(): String {
        val userDto = findUserByFirebase()

        if (userDto.imageUrl == null) {
            throw ImageUrlNotFoundException()
        }

        return userDto.imageUrl!!
    }

    @Transactional
    fun changeBio(bio: String) {
        val userDto = findUserByFirebase()

        userDto.bio = bio
        userRepository.save(userDto.toEntity())
    }

    fun getUserFullInfo() : UserFullResponse {
        return findUserByFirebase().fullResponse()
    }

    @Transactional
    fun switchUserCategory(categoryName: String) : SwitchCategoryResponse{
        val userDTO = findUserByFirebase()
        val category = categoryRepository.getCategoriesByName(categoryName) ?: throw CategoryNotFoundException()

        var result : String?

        if(userDTO.categories.any { it.name == categoryName }) {
            userDTO.categories.removeIf{it.name == categoryName}
            result = "REMOVED"
        } else {
            userDTO.categories.add(category.toDto())
            result = "ADDED"
        }

        userRepository.save(userDTO.toEntity())
        return SwitchCategoryResponse(result)
    }
}
