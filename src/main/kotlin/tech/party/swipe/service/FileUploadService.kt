package tech.party.swipe.service

import io.jsonwebtoken.Header
import jakarta.servlet.http.HttpServletRequest
import org.apache.tomcat.util.http.fileupload.MultipartStream
import org.apache.tomcat.util.http.fileupload.util.LimitedInputStream
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.util.MimeType
import org.springframework.web.multipart.MultipartFile
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetUrlRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import tech.party.swipe.configuration.ImageBucketConfigurationProperties
import tech.party.swipe.data.UploadedFileLinkData
import tech.party.swipe.enums.UploadType
import tech.party.swipe.exception.FileSizeExceededException
import tech.party.swipe.exception.NoFileProvidedException
import tech.party.swipe.utils.FileConstants.UNLIMITED
import tech.party.swipe.utils.UserUtils.getUserInfo
import java.io.IOException
import java.io.InputStream
import java.net.URI
import java.util.*

@Service
class FileUploadService(
    private val properties: ImageBucketConfigurationProperties,
    private val s3Client: S3Client,
) {
    fun upload(
        type: UploadType,
        file: MultipartFile,
        httpServletRequest: HttpServletRequest,

        ): UploadedFileLinkData {
        return processFile(file, type, httpServletRequest)
    }

    private fun processFile(
        file: MultipartFile,
        type: UploadType,
        httpServletRequest: HttpServletRequest,
    ): UploadedFileLinkData {
        val mimeType = MimeType.valueOf(httpServletRequest.contentType)
        val userId = SecurityContextHolder.getContext().authentication.getUserInfo()
        val key = "${type.folderName}/$userId/${UUID.randomUUID()}"

        val putRequest = PutObjectRequest.builder()
            .bucket(properties.usersContentBucket)
            .key(key)
            .contentType(mimeType.toString())
            .contentDisposition(INLINE_CONTENT_DISPOSITION)
            .acl(PUBLIC_READ_ACL)
            .build()

        s3Client.putObject(putRequest, RequestBody.fromContentProvider({ file.inputStream }, mimeType.toString()))


        val getUrlRequest = GetUrlRequest.builder().endpoint(URI.create("https://storage.yandexcloud.net")).bucket(properties.usersContentBucket).key(key).build()
        return UploadedFileLinkData(s3Client.utilities().getUrl(getUrlRequest).toString())
    }

    companion object {
        private const val PUBLIC_READ_ACL = "public-read"
        private const val INLINE_CONTENT_DISPOSITION = "inline"
    }
}