package tech.party.swipe.service

import org.springframework.stereotype.Service
import tech.party.swipe.dto.CategoryDto
import tech.party.swipe.repository.CategoryRepository

@Service
class CategoryService(private val repository: CategoryRepository) {
    fun getAllCategories(): List<CategoryDto> = repository.findAll().map { category ->
        category.toDto()
    }
}
