package tech.party.swipe.service

import jakarta.servlet.http.HttpServletRequest
import org.apache.tomcat.util.http.fileupload.MultipartStream
import org.apache.tomcat.util.http.fileupload.util.LimitedInputStream
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.util.MimeType
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetUrlRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import tech.party.swipe.configuration.ImageBucketConfigurationProperties
import tech.party.swipe.data.UploadedFileLinkData
import tech.party.swipe.enums.UploadType
import tech.party.swipe.exception.FileSizeExceededException
import tech.party.swipe.exception.NoFileProvidedException
import tech.party.swipe.utils.FileConstants.UNLIMITED
import tech.party.swipe.utils.UserUtils.getUserInfo
import java.io.IOException
import java.io.InputStream
import java.util.*

@Service
class FileMultipartUploadService(
    private val properties: ImageBucketConfigurationProperties,
    private val s3Client: S3Client,
) {

    // Using this complicated method instead of Spring capabilities to upload file as a stream to S3
    // without intermediate temp file on disk
    fun upload(
        type: UploadType,
        request: HttpServletRequest,
    ): UploadedFileLinkData {
        val boundary = extractBoundary(request)
        try {
            val multipartStream = MultipartStream(request.inputStream, boundary.toByteArray(), 1024, null)
            var nextPart: Boolean = multipartStream.skipPreamble()

            while (nextPart) {
                val header: String = multipartStream.readHeaders()
                // if input is file
                if (header.contains("filename")) return processFile(header, multipartStream, type)

                nextPart = multipartStream.readBoundary()
            }
        } catch (e: IOException) {
            throw RuntimeException("Error uploading the file: ${e.localizedMessage}")
        }
        throw NoFileProvidedException()
    }

    private fun processFile(
        header: String,
        multipartStream: MultipartStream,
        type: UploadType,
    ): UploadedFileLinkData {
        val extractMimeType = extractMimeType(header)
        val mimeType = extractMimeType.toString()
        val userId = SecurityContextHolder.getContext().authentication.getUserInfo()
        val key = "${type.folderName}/$userId/${UUID.randomUUID()}"

        val putRequest = PutObjectRequest.builder()
            .bucket(properties.usersContentBucket)
            .key(key)
            .contentType(mimeType)
            .contentDisposition(INLINE_CONTENT_DISPOSITION)
            .acl(PUBLIC_READ_ACL)
            .build()

        multipartStream.newInputStream().use {
            val inputStream = getInputStream(type, it)
            s3Client.putObject(putRequest, RequestBody.fromContentProvider({ inputStream }, mimeType))
        }

        val getUrlRequest = GetUrlRequest.builder().bucket(properties.usersContentBucket).key(key).build()
        return UploadedFileLinkData(s3Client.utilities().getUrl(getUrlRequest).toString())
    }

    private fun getInputStream(
        type: UploadType,
        originalInputStream: MultipartStream.ItemInputStream,
    ): InputStream {
        return when (type.fileSizeLimitInBytes) {
            UNLIMITED -> originalInputStream
            else -> object : LimitedInputStream(originalInputStream, type.fileSizeLimitInBytes) {
                override fun raiseError(pSizeMax: Long, pCount: Long) {
                    throw FileSizeExceededException(pSizeMax)
                }
            }
        }
    }

    private fun extractMimeType(header: String) = header.split("\n")
        .map { it.trim().split(":") }
        .filter { it.first().lowercase() == CONTENT_TYPE_KEY }
        .map { MimeType.valueOf(it.last()) }
        .first()

    private fun extractBoundary(request: HttpServletRequest): String {
        val partStartIndex = request.contentType.indexOf(BOUNDARY_KEY) + BOUNDARY_KEY.length
        return request.contentType.substring(partStartIndex)
    }

    companion object {
        private const val PUBLIC_READ_ACL = "public-read"
        private const val INLINE_CONTENT_DISPOSITION = "inline"
        private const val CONTENT_TYPE_KEY = "content-type"
        private const val BOUNDARY_KEY = "boundary="
    }
}
