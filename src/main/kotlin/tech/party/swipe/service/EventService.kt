package tech.party.swipe.service

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import tech.party.swipe.dto.EventDto
import tech.party.swipe.exception.AuthenticationException
import tech.party.swipe.exception.CategoryNotFoundException
import tech.party.swipe.mapper.EventMapper
import tech.party.swipe.mapper.EventMapper.entityToEventReturnResponse
import tech.party.swipe.repository.CategoryRepository
import tech.party.swipe.repository.EventRepository
import tech.party.swipe.repository.ViewsRepository
import tech.party.swipe.restdata.EventCreationRequest
import tech.party.swipe.restdata.EventReturnResponse
import tech.party.swipe.restdata.EventUpdateRequest
import tech.party.swipe.security.JWTToken
import java.time.LocalDate
import java.time.LocalDateTime

@Service
class EventService(
    private val userService: UserService,
    private val eventRepository: EventRepository,
    private val categoryRepository: CategoryRepository,
    private val viewsRepository: ViewsRepository
) {

    fun getAllEvents(): List<EventReturnResponse> =
        eventRepository.findAll().map{ entityToEventReturnResponse(it,it.id?.let{viewsRepository.findAllByEventId(it).size}?:0) }

    fun addEvent(eventCreationRequest: EventCreationRequest): EventReturnResponse {
        val user = userService.findUserByFirebase()

        val categories = eventCreationRequest.tags.map {
                category ->
            categoryRepository.getCategoriesByName(category) ?: throw CategoryNotFoundException()
        }

        return EventMapper.entityToEventReturnResponse(
            eventRepository.save(
                tech.party.swipe.entity.Event(
                    location = eventCreationRequest.locate,
                    date = eventCreationRequest.date,
                    description = eventCreationRequest.discript,
                    picture = eventCreationRequest.image_id,
                    name = eventCreationRequest.name,
                    owner = user.toEntity(),
                    categories = categories,
                ),
            ),
        )
    }

    fun getEventsByUserId(): List<EventReturnResponse> {
        val user = userService.findUserByFirebase()
        val events = eventRepository.getAllByOwnerId(user.id!!)
            .map{entityToEventReturnResponse(it,it.id?.let{viewsRepository.findAllByEventId(it).size}?:0)}

        events.forEach{it.addViews(viewsRepository.findAllByEventId(it.id).size)}

        return events
    }

    fun updateEvent(updateRequest: EventUpdateRequest): EventReturnResponse {
        val event = eventRepository.getById(updateRequest.id)
        val user = userService.findUserByFirebase()

        if (event.owner.id != user.id) {
            throw AuthenticationException("This event is not accessible by that user")
        }

        val categories = updateRequest.tags.map {
                category ->
            categoryRepository.getCategoriesByName(category) ?: throw CategoryNotFoundException()
        }

        return EventMapper.entityToEventReturnResponse(
            eventRepository.save(
                tech.party.swipe.entity.Event(
                    id = event.id,
                    location = updateRequest.locate,
                    date = updateRequest.date,
                    description = event.description,
                    picture = event.picture,
                    name = event.name,
                    owner = event.owner,
                    categories = categories,
                ),
            ),
        )
    }

    fun deleteOldEvents(): List<Long> {
        val localDate = LocalDateTime.now()
        val oldEvents = eventRepository.findAll().map { it.toDto() }.filter { event ->
            event.date < localDate
        }

        oldEvents.forEach { event -> eventRepository.deleteEventsById(
                event.id!!
            )}

        return oldEvents.map { it.id ?: 0 }
    }
}
