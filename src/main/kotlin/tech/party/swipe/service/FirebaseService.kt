package tech.party.swipe.service

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Service

@Service
class FirebaseService {
    @PostConstruct
    fun init() {
        val options: FirebaseOptions = FirebaseOptions.builder().setCredentials(
            GoogleCredentials.fromStream(
                Thread.currentThread().contextClassLoader.getResourceAsStream(
                    "firebase_config.json",
                ),
            ),
        ).build()
        if (FirebaseApp.getApps().isEmpty()) {
            FirebaseApp.initializeApp(options)
        }
    }
}
