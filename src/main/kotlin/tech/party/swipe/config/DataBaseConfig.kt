package tech.party.swipe.config

import com.google.firebase.database.core.DatabaseConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import tech.party.swipe.service.EventService
import tech.party.swipe.service.MatchService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

@Component
@Order(0)
class DataBaseConfig(
    private val matchesService: MatchService,
    private val eventService: EventService,
) : ApplicationListener<ApplicationReadyEvent> {
    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        val logger: Logger = LoggerFactory.getLogger(DatabaseConfig::class.java)
        logger.info("Event cleaner initialized")
        logger.info("Matches updater initialized")

        val scheduledExecutor = Executors.newScheduledThreadPool(1)
        scheduledExecutor.scheduleAtFixedRate({
            logger.info("Old event removing started")

//            eventService.deleteOldEvents().forEach { logger.info(
//                "deleted event with id$it"
//            ) }

            logger.info("Old event removing finished")
            logger.info("Matches updating started")

            matchesService.getAllUserMatches()

            logger.info("Matches updating finished")
        }, 5, 300, TimeUnit.SECONDS)

        Thread.sleep(1000)
    }
}
