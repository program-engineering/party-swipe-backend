package tech.party.swipe.exception

import org.springframework.core.Ordered

abstract class ControllerAdviceOrder(parent: ControllerAdviceOrder?) {
    val controllerAdviceOrder: Int = parent?.controllerAdviceOrder?.dec() ?: Ordered.LOWEST_PRECEDENCE
}
