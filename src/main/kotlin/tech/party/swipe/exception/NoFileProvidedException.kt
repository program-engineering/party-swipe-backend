package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class NoFileProvidedException : AbstractPartySwipeException(
    errorCode = NoFileProvidedException::class.simpleName!!,
    status = HttpStatus.BAD_REQUEST,
    payload = mapOf(),
)
