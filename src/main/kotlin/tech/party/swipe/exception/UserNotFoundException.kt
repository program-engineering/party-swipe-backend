package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class UserNotFoundException : AbstractPartySwipeException(
    errorCode = UserNotFoundException::class.simpleName!!,
    status = HttpStatus.UNAUTHORIZED,
    payload = mapOf(),
)
