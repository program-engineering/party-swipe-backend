package tech.party.swipe.exception

import org.springframework.http.HttpStatusCode

abstract class AbstractPartySwipeException(
    val errorCode: String,
    val status: HttpStatusCode,
    val payload: Map<String, Any?> = mutableMapOf(),
    cause: Throwable? = null,
) : RuntimeException(errorCode, cause) {
    companion object : ControllerAdviceOrder(null)
}
