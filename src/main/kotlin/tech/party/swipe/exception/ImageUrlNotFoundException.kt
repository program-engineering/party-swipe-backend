package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class ImageUrlNotFoundException : AbstractPartySwipeException(
    errorCode = ImageUrlNotFoundException::class.simpleName!!,
    status = HttpStatus.NOT_FOUND,
)
