package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class FileSizeExceededException(maxSize: Long) : AbstractPartySwipeException(
    errorCode = FileSizeExceededException::class.simpleName!!,
    status = HttpStatus.BAD_REQUEST,
    payload = mapOf("maxSize" to maxSize),
)
