package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class CategoryNotFoundException : AbstractPartySwipeException(
    errorCode = CategoryNotFoundException::class.simpleName!!,
    status = HttpStatus.BAD_REQUEST,
    payload = mapOf(),
)
