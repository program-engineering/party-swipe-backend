package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class EventNotFoundException : AbstractPartySwipeException(
    errorCode = EventNotFoundException::class.simpleName!!,
    status = HttpStatus.NOT_FOUND,
    payload = mapOf(),
)
