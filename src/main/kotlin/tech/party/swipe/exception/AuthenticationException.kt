package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class AuthenticationException(message: String) : AbstractPartySwipeException(
    errorCode = AuthenticationException::class.simpleName!!,
    status = HttpStatus.UNAUTHORIZED,
    payload = mapOf(),
)
