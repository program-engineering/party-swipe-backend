package tech.party.swipe.exception

import org.springframework.http.HttpStatus

class WrongFileTypeException(type: String) : AbstractPartySwipeException(
    errorCode = WrongFileTypeException::class.simpleName!!,
    status = HttpStatus.BAD_REQUEST,
    payload = mapOf("type" to type),
)
