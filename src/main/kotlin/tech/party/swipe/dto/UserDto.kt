package tech.party.swipe.dto

import tech.party.swipe.entity.User
import tech.party.swipe.restdata.UserFullResponse
import tech.party.swipe.restdata.UserResponseData
import java.time.LocalDate
import java.util.*

data class UserDto(
    val id: Long? = null,
    val email: String,
    val dateOfBirth: LocalDate,
    val firebaseId: String,
    val name: String,
    val categories: MutableSet<CategoryDto>,
    var imageUrl: String? = null,
    var bio: String = "С пивом к счастью!",
) {
    fun toEntity(): User = User(
        id = id,
        email = email,
        dateOfBirth = dateOfBirth,
        firebaseId = firebaseId,
        name = name,
        categories = categories.toList().map { it.toEntity() },
        imageUrl = imageUrl,
        bio = bio,
    )

    fun toResponse(): UserResponseData = UserResponseData(
        username = name,
        userImageUrl = imageUrl,
        dateOfBirth = dateOfBirth,
        bio = bio,
    )

    fun fullResponse(): UserFullResponse = UserFullResponse(
        name = name,
        imageUrl = imageUrl,
        dateOfBirth = dateOfBirth,
        bio = bio,
        categories = categories.toList().map { it.name }
    )
}
