package tech.party.swipe.dto

data class ChangeUserBioRequest(
    val bio: String
)
