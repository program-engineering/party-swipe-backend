package tech.party.swipe.dto

import tech.party.swipe.entity.View

class ViewDto(
    val id: Long? = null,
    val event: EventDto,
    val user: UserDto,
) {
    fun toEntity(): View = View(
        id = id,
        event = event.toEntity(),
        user = user.toEntity(),
    )
}
