package tech.party.swipe.dto

import tech.party.swipe.entity.Like

class LikeDto(
    val id: Long? = null,
    val event: EventDto,
    val user: UserDto,
) {
    fun toEntity(): Like = Like(
        id = id,
        event = event.toEntity(),
        user = user.toEntity(),
    )
}
