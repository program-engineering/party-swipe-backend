package tech.party.swipe.dto

import tech.party.swipe.entity.Category

data class CategoryDto(
    val id: Long? = null,
    val name: String,
) {
    fun toEntity(): Category = Category(
        id = id,
        name = name,
    )
}
