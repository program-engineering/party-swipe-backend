package tech.party.swipe.dto

import java.time.LocalDate

data class UserRegisterDto(
    val name: String,
    val dateOfBirth: LocalDate,
)
