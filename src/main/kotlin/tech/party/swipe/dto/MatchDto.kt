package tech.party.swipe.dto

import jakarta.persistence.*
import tech.party.swipe.entity.Match
import tech.party.swipe.mapper.EventMapper.entityToEventReturnResponse
import tech.party.swipe.restdata.MatchResponse

class MatchDto(
    val id: Long? = null,
    val user1: UserDto,
    val user2: UserDto,
    val event: EventDto,
) {
    fun toEntity(): Match = Match(
        id = id,
        user1 = user1.toEntity(),
        user2 = user2.toEntity(),
        event = event.toEntity(),
    )
    @Deprecated("now with views")
    fun toResponse(): MatchResponse = MatchResponse(
        id = id!!,
        event = entityToEventReturnResponse(event.toEntity()),
        userBio = user2.toResponse(),
    )
    fun toResponse(views: Int): MatchResponse = MatchResponse(
        id = id!!,
        event = entityToEventReturnResponse(event.toEntity(),views),
        userBio = user2.toResponse(),
    )

}
