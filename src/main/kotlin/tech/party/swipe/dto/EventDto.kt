package tech.party.swipe.dto

import tech.party.swipe.entity.Event
import java.time.LocalDateTime

data class EventDto(
    val id: Long? = null,
    val name: String,
    val image_id: String,
    val date: LocalDateTime,
    val locate: String,
    val description: String,
    val categories: List<CategoryDto>,
    val owner: UserDto,
) {
    fun toEntity(): Event = Event(
        id = id,
        name = name,
        date = date,
        location = locate,
        description = description,
        categories = categories.map { it.toEntity() },
        picture = image_id,
        owner = owner.toEntity(),
    )
}
