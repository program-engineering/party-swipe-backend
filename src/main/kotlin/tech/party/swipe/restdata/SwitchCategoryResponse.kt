package tech.party.swipe.restdata

data class SwitchCategoryResponse(
    val result: String
)
