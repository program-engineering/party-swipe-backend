package tech.party.swipe.restdata

class MatchResponse(
    val id: Long,
    val event: EventReturnResponse,
    val userBio: UserResponseData,
)
