package tech.party.swipe.restdata

import java.time.LocalDateTime

data class EventCreationRequest(
    val name: String,
    val image_id: String,
    val date: LocalDateTime,
    val count_likes: Int,
    val locate: String,
    val discript: String,
    val tags: List<String>,
)
