package tech.party.swipe.restdata

import java.time.LocalDate

class UserFullResponse(
    val dateOfBirth: LocalDate,
    val name: String,
    val categories: List<String>,
    var imageUrl: String? = null,
    var bio: String = "С пивом к счастью!",
)