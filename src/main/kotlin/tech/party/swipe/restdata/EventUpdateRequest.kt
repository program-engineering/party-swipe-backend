package tech.party.swipe.restdata

import java.time.LocalDateTime

data class EventUpdateRequest(
    val id: Long,
    val date: LocalDateTime = LocalDateTime.now(),
    val locate: String,
    val tags: List<String>,
)
