package tech.party.swipe.restdata

import java.time.LocalDate

class UserResponseData(
    val username: String,
    val userImageUrl: String?,
    val dateOfBirth: LocalDate,
    val bio: String,
)
