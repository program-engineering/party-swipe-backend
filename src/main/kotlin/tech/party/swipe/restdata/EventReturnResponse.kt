package tech.party.swipe.restdata

import java.time.LocalDateTime

data class EventReturnResponse(
    val id: Long,
    val name: String,
    val image_id: String?,
    val date: LocalDateTime = LocalDateTime.now(),
    val countLikes: Int,
    val locate: String,
    val discript: String,
    val tags: List<String>,
    val ownerId: Long,
    var views: Int = 0
    ) {
    fun addViews(views: Int) {
        this.views = views
    }
}
