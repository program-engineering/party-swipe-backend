package tech.party.swipe.enums

import tech.party.swipe.exception.WrongFileTypeException
import tech.party.swipe.utils.FileConstants.BYTES_IN_MB
import tech.party.swipe.utils.FileConstants.UNLIMITED

enum class UploadType(
    val pathSegment: String,
    val folderName: String,
    val fileSizeLimitInBytes: Long = UNLIMITED,
) {
    USER_PICTURE("user-picture", "user-picture", 5 * BYTES_IN_MB),
    EVENT_PICTURE("event-picture", "event-picture", 5 * BYTES_IN_MB),
    ;

    companion object {
        fun getByPathSegment(pathSegment: String): UploadType =
            values().find { it.pathSegment == pathSegment }
                ?: throw WrongFileTypeException(pathSegment)
    }
}
