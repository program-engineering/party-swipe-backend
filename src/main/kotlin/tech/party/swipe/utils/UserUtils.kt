package tech.party.swipe.utils

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import tech.party.swipe.security.JWTToken

object UserUtils {
    fun Authentication.getUserInfo(): String = (this as JWTToken).getSubject()

    fun extractFirebaseIdFromSecurityContext(): String {
        return (SecurityContextHolder.getContext().authentication as JWTToken).principal
    }
}
