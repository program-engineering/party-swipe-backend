package tech.party.swipe.utils

object FileConstants {
    const val UNLIMITED = -1L
    const val BYTES_IN_KB = 1024L
    const val BYTES_IN_MB = 1024L * BYTES_IN_KB
}
