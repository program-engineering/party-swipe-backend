package tech.party.swipe.security

import org.springframework.core.convert.converter.Converter
import org.springframework.security.oauth2.jwt.Jwt

class JWTConverter :
    Converter<Jwt, JWTToken> {

    override fun convert(source: Jwt): JWTToken {
        val firebaseId = source.getClaim<String>("user_id")
        return JWTToken(
            jwt = source,
            firebaseUserId = firebaseId,
        )
    }
}
