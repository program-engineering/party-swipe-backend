package tech.party.swipe.security

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseToken
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.AbstractOAuth2TokenAuthenticationToken

class JWTToken(
    jwt: Jwt,
    private val firebaseUserId: String,
) : AbstractOAuth2TokenAuthenticationToken<Jwt>(jwt) {
    init {
        isAuthenticated = true
    }

    override fun getTokenAttributes(): MutableMap<String, Any> {
        return token.claims
    }

    fun getSubject(): String {
        return token.subject
    }

    override fun getCredentials(): FirebaseToken {
        return FirebaseAuth.getInstance().verifyIdToken(token.tokenValue)
    }

    override fun getPrincipal(): String {
        return firebaseUserId
    }
}
