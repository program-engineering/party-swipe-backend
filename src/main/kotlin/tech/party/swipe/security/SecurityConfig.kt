package tech.party.swipe.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.SecurityFilterChain

@EnableWebSecurity
@Configuration
class SecurityConfig() {

    @Bean
    fun apiFilterChain(http: HttpSecurity): SecurityFilterChain {
        http.securityMatcher("/api/**").authorizeHttpRequests { request ->
            request.anyRequest().authenticated()
        }.oauth2ResourceServer { resourceServer ->
            resourceServer.jwt { jwt ->
                jwt.jwtAuthenticationConverter(JWTConverter())
            }
        }
        return http.build()
    }
}
