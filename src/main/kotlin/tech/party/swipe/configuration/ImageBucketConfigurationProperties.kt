package tech.party.swipe.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "images.bucket")
data class ImageBucketConfigurationProperties(
    val accessKeyId: String,
    val secretAccessKey: String,
    val region: String,
    val usersContentBucket: String,
)
