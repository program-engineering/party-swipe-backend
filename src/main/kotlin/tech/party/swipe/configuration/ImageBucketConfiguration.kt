package tech.party.swipe.configuration

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.endpoints.S3EndpointParams
import software.amazon.awssdk.services.s3.endpoints.S3EndpointProvider
import java.net.URI

@Configuration
@EnableConfigurationProperties(ImageBucketConfigurationProperties::class)
class ImageBucketConfiguration {

    @Bean
    fun s3client(awsConfigurationProperties: ImageBucketConfigurationProperties): S3Client =
        S3Client.builder()
            .endpointProvider {
                S3EndpointProvider.defaultProvider().resolveEndpoint(
                    S3EndpointParams.builder().endpoint("https://storage.yandexcloud.net")
                        .bucket("party-swipe-bucket")
                        .region(Region.of(awsConfigurationProperties.region)).build()
                )
            }
            .region(Region.of(awsConfigurationProperties.region))
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create(
                        awsConfigurationProperties.accessKeyId,
                        awsConfigurationProperties.secretAccessKey,
                    ),
                ),
            )
            .build()
}