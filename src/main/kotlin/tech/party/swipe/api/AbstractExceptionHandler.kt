package tech.party.swipe.api

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import tech.party.swipe.exception.AbstractPartySwipeException

@ControllerAdvice
@Order
class AbstractExceptionHandler : ResponseEntityExceptionHandler(), Ordered {
    @ExceptionHandler(AbstractPartySwipeException::class)
    fun handleAbstractApiException(ex: AbstractPartySwipeException): ResponseEntity<ErrorResponseBody> {
        return ResponseEntity(ErrorResponseBody(ex.message), ex.status)
    }

    override fun getOrder() = AbstractPartySwipeException.controllerAdviceOrder
}
