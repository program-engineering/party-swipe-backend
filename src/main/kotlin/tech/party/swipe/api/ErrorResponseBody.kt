package tech.party.swipe.api

data class ErrorResponseBody(val reason: String?)
