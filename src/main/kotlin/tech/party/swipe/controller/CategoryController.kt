package tech.party.swipe.controller

import org.springframework.web.bind.annotation.*
import tech.party.swipe.restdata.SwitchCategoryResponse
import tech.party.swipe.service.CategoryService
import tech.party.swipe.service.UserService

@RestController
@RequestMapping("/api/categories")
class CategoryController(val categoryService: CategoryService, val userService: UserService) {
    @GetMapping
    fun allCategoriesStringList(): List<String> {
        return categoryService.getAllCategories().map { category -> category.name }
    }

    @GetMapping("/user-categories")
    fun userCategoriesStringList(): List<String> {
        return userService.getUserCategories().map { category -> category.name }
    }

    @PostMapping("/user-categories/{category}")
    fun addUserCategory(
        @PathVariable category: String,
    ): String {
        return userService.addUserCategory(category)
    }

    @DeleteMapping("/user-categories/{category}")
    fun deleteUserCategory(
        @PathVariable category: String,
    ): String {
        return userService.deleteUserCategory(category)
    }

    @PutMapping("/user-categories/{category}/switch")
    fun switchCategory(@PathVariable category: String) : SwitchCategoryResponse {
        return userService.switchUserCategory(category)
    }
}
