package tech.party.swipe.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import tech.party.swipe.restdata.MatchResponse
import tech.party.swipe.service.MatchService

@RestController
@RequestMapping("/api/matches")
class MatchesController(private val matchService: MatchService) {

    @GetMapping
    fun getUserMatches(): List<MatchResponse> = matchService.getUserMatches()
}
