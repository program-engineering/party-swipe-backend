package tech.party.swipe.controller

import org.springframework.web.bind.annotation.*
import tech.party.swipe.restdata.EventReturnResponse
import tech.party.swipe.service.EventReactionService

@RestController
@RequestMapping("/api/reactions")
class EventReactionController(private val eventReactionService: EventReactionService) {
    @GetMapping("/get-user-likes")
    fun getUserLike(): List<EventReturnResponse> = eventReactionService.getLikedEvents()

    @PutMapping("/like")
    fun likeEvent(@RequestParam eventId: Long) = eventReactionService.likeEvent(eventId = eventId)

    @PutMapping("/view")
    fun viewEvent(@RequestParam eventId: Long) = eventReactionService.viewEvent(eventId = eventId)
}
