package tech.party.swipe.controller

import lombok.AllArgsConstructor
import org.springframework.web.bind.annotation.*
import tech.party.swipe.dto.EventDto
import tech.party.swipe.restdata.EventCreationRequest
import tech.party.swipe.restdata.EventReturnResponse
import tech.party.swipe.restdata.EventUpdateRequest
import tech.party.swipe.service.EventService

@RestController
@AllArgsConstructor
@RequestMapping("/api/events")
class EventController(val eventService: EventService) {

    @GetMapping
    fun getListAllEvents(): List<EventReturnResponse> {
        return eventService.getAllEvents()
    }

    @PostMapping("/create")
    fun addNewEvent(
        @RequestBody eventCreationRequest: EventCreationRequest,
    ): EventReturnResponse {
        return eventService.addEvent(eventCreationRequest)
    }

    @GetMapping("/user-events")
    fun getListEventsByUser(): List<EventReturnResponse> {
        return eventService.getEventsByUserId()
    }

    @PutMapping("/update")
    fun updateEvent(@RequestBody updateRequest: EventUpdateRequest): EventReturnResponse {
        return eventService.updateEvent(updateRequest)
    }
}
