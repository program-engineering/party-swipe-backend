package tech.party.swipe.controller

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import tech.party.swipe.dto.ChangeUserBioRequest
import tech.party.swipe.dto.UserDto
import tech.party.swipe.dto.UserRegisterDto
import tech.party.swipe.restdata.UserFullResponse
import tech.party.swipe.restdata.UserResponseData
import tech.party.swipe.service.UserService

@RestController
@RequestMapping("/api/user")
class UserController(private val userService: UserService) {

    @GetMapping("/me")
    fun getCalculationsList(): String = SecurityContextHolder.getContext().authentication.principal.toString()

    @PutMapping("/register")
    fun registerUser(request: UserRegisterDto): UserResponseData = userService.addUser(request)

    @GetMapping("/image")
    fun getUserImageUrl(): String? {
        return userService.getUserImageUrl()
    }

    @PostMapping("/image/{image_url}")
    fun addUserImageUrl(@PathVariable image_url: String): String {
        return userService.addUserImageUrl(image_url)
    }

    @PutMapping("/image/{image_url}")
    fun updateUserImageUrl(@PathVariable image_url: String): String {
        return userService.updateUserImageUrl(image_url)
    }

    @DeleteMapping("/image")
    fun deleteUserImageUrl(): String? {
        return userService.deleteUserImageUrl()
    }

    @PostMapping("/bio")
    fun changeBio(@RequestBody request: ChangeUserBioRequest) {
        userService.changeBio(request.bio)
    }

    @GetMapping("/info")
    fun getFullUserInfo() : UserFullResponse {
        return userService.getUserFullInfo()
    }
}
