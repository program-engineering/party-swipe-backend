package tech.party.swipe.controller

import jakarta.servlet.http.HttpServletRequest
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import tech.party.swipe.data.UploadedFileLinkData
import tech.party.swipe.enums.UploadType
import tech.party.swipe.service.FileUploadService

@RestController
@RequestMapping("/api/files/v1")
class FileUploadController(
    private val fileUploadService: FileUploadService,
) {
    @PutMapping("/{uploadType}")
    fun upload(
        @PathVariable uploadType: String,
        @RequestParam("file") file: MultipartFile,
        httpServletRequest: HttpServletRequest,
    ): UploadedFileLinkData {
        val type = UploadType.getByPathSegment(uploadType)
        return fileUploadService.upload(type, file, httpServletRequest)
    }
}