rootProject.name = "PartySwipe"

dependencyResolutionManagement {
    repositories {
        mavenCentral()
        maven {
            url = uri("https://packages.confluent.io/maven")
        }
    }

    versionCatalogs {
        create("deps") {

            library("springBootStarterTest", "org.springframework.boot", "spring-boot-starter-test").withoutVersion()
            library("mockitoCore", "org.mockito.kotlin:mockito-kotlin:5.0.0")
            library("mockitoInline", "org.mockito:mockito-inline:2.13.0")
            library("junitJupiter", "org.testcontainers", "junit-jupiter").withoutVersion()
            library("embeddedDatabaseSpringTest", "io.zonky.test:embedded-database-spring-test:2.2.0")
            library("postgresql", "org.testcontainers", "postgresql").withoutVersion()
            bundle(
                "test",
                listOf(
                    "springBootStarterTest",
                    "mockitoCore",
                    "mockitoInline",
                    "junitJupiter",
                    "embeddedDatabaseSpringTest",
                    "postgresql",
                ),
            )

            library("springBootStarterDataJPA", "org.springframework.boot", "spring-boot-starter-data-jpa").withoutVersion()
            library("flywayCore", "org.flywaydb", "flyway-core").withoutVersion()
            bundle("db", listOf("springBootStarterDataJPA", "flywayCore"))
        }
    }
}
